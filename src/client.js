'use strict';

var util = require('util'),
    _ = require('lodash'),
    Bunyan = require('bunyan'),
    Zmq = require('zmq'),
    UUID = require('uuid'),
    Bluebird = require('bluebird'),
    msgpack = require('msgpack'),
    request = require('request');

var instance = false;

class Client {
  static discoverQuadraBackends() {
    var path = util.format(
      '/v1/kv/quadra/pools/%s/instances?recurse',
      process.env.BACKEND_POOL
    );

    return new Bluebird((resolve, reject) => {
      request({
        url: util.format(
          'https://%s:%s%s',
          process.env.QUADRA_CONSUL_HOST,
          process.env.QUADRA_CONSUL_PORT,
          path
        ),
        rejectUnauthorized: false,
        method: 'GET'
      }, function (err, resp, body) {
        if (err) {
          return reject(err);
        }

        if (body) {
          var kvs = body;

          if (_.isString(kvs)) {
            kvs = JSON.parse(kvs);
          }

          var ips = kvs.filter((kv) => {
            return kv.Value;
          }).map((kv) => {
            return util.format(
              'tcp://%s:5671',
              JSON.parse(new Buffer(kv.Value, 'base64').toString()).nat_ip
            );
          });

          return resolve(ips);
        }
      });
    });
  }

  static discover() {
    if (!process.env.QUADRA_CONSUL_HOST || !process.env.BACKEND_POOL) {
      return Bluebird.resolve([
        'tcp://localhost:5671'
      ]);
    }

    return this.discoverQuadraBackends();
  }

  static instance() {
    if (!instance) {
      instance = new Client();
    }

    return instance;
  }

  constructor() {
    // Array of hosts
    this.hosts = [];

    // Hash of requests keyed on id
    this.requests = {};

    this.logger = Bunyan.createLogger({
      name: 'Kraken Client',
      level: process.env.LOG_LEVEL ? parseInt(process.env.LOG_LEVEL, 10) : 30
    });

    this.socket = Zmq.socket('dealer');
    this.socket.identity = UUID.v4();
    this.socket.on('message', this.receive.bind(this));

    this.refreshHosts();

    // For Quadra-ing
    // Refresh hosts every 1 second
    this.refreshInterval = setInterval(() => {
      this.refreshHosts();
    }, 5000);
  }

  receive() {
    var args = {
      id: arguments[0].toString(),
      error: msgpack.unpack(arguments[1]),
      data: msgpack.unpack(arguments[2])
    };

    // Error from the hash received
    if (args.error) {
      var error = new Error(args.error.message);
      error.type = args.error.type;
      error.code = args.error.code || 500;

      args.error = error;
    }

    var request = this.requests[args.id];

    console.log({ id: args.id, task: request.task }, 'Recieved request');

    request.callback(args.error, args.data);

    // Were done, lets remove it
    delete this.requests[args.id];
  }

  dispatch(task, params, callback) {
    params = params || null;

    var id = UUID.v4();

    this.requests[id] = {
      task: task,
      params: params,
      callback: callback
    };

    this.socket.send([id, task, msgpack.pack(params)]);
  }

  close() {
    clearInterval(this.refreshInterval);

    this.hosts.forEach((host) => {
      this.socket.disconnect(host);
    });
  }

  refreshHosts() {
    this.constructor.discover()

    .tap((hosts) => {
      this.logger.debug(
        {
          hosts: hosts
        },
        'Refreshing hosts'
      );
    })

    .then((hosts) => {
      _.difference(this.hosts, hosts).forEach((host) => {
        this.socket.disconnect(host);
        this.hosts = _.without(this.hosts, host);
      });

      _.difference(hosts, this.hosts).forEach((host) => {
        this.socket.connect(host);
        this.hosts.push(host);
      });
    });
  }
};

module.exports = Client;