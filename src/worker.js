'use strict';

var util = require('util'),
    _ = require('lodash'),
    Zmq = require('zmq'),
    Bluebird = require('bluebird'),
    msgpack = require('msgpack');

var instance = false;

class Worker {
  static instance() {
    if (!instance) {
      instance = new Worker();
    }

    return instance;
  }

  constructor() {
    this.tasks = {};

    this.socket = Zmq.socket('router');
    this.socket.bindSync(util.format('tcp://*:%d', parseInt(process.env.PORT, 10) || 5671));
    this.socket.on('message', this.receive.bind(this));
  }

  receive() {
    var args = {
      identity: arguments[0].toString(),
      id: arguments[1].toString(),
      task: arguments[2].toString(),
      params: msgpack.unpack(arguments[3])
    };

    console.log({ message: args }, 'Recieved request');

    this.process(args);
  }

  dispatch(identity, requestId, error, data) {
    this.socket.send(Array.call(null, identity, requestId, msgpack.pack(error), msgpack.pack(data)));
  }

  process(args) {
    return Bluebird.resolve(args)
    .bind(this)
    .then((args) => {
      return this.tasks[args.task](args.params);
    })
    .then((response) => {
      this.dispatch(args.identity, args.id, null, response);
    })
    .catch((error) => {
      console.error('Got error', error);
      this.dispatch(args.identity, args.id, this.processError(error), null);
    });
  }

  processError(error) {
    return {
      code: error.code || 400,
      message: error.message,
      type: error.name
    };
  }

  registerCollection(collection, name) {
    [
      'list',
      'read',
      'create',
      'update',
      'delete'
    ].forEach((action) => {
      var fn = _.get(collection, action);

      if (fn && _.isFunction(fn)) {
        var task = util.format('task:%s:%s', name || collection.constructor.name, action);

        this.tasks[task] = fn;
      }
    });
  }
};

module.exports = Worker;